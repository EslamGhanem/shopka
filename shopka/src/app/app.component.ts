import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { filter, map, mergeMap } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'shopka';
  private pageTitle = '';

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private titleService: Title
  ) {
    this.getPageTitle();
  }

  /**
   * Subscribes to the router events and updates the page title with the translated value
   * of the current route's title data.
   * Uses the 'MENU.' prefix to conform to the translation key convention.
   */

  getPageTitle(): void {
    this.router.events
      .pipe(
        filter((e) => e instanceof NavigationEnd),
        map(() => {
          let route = this.activatedRoute.firstChild;
          let child = route;
          while (child) {
            if (child.firstChild) {
              child = child.firstChild;
              route = child;
            } else {
              child = null;
            }
          }
          return route;
        }),
        mergeMap((route) => route!.data)
      )
      .subscribe((data) => {
        this.pageTitle = `${data['title']}`;
        this.setPageTitle();
      });
  }

  /**
   * Uses the translation service to get the translated values for the 'MENU.DEALCOM'
   * and 'this.pageTitle' keys, concatenates them with a separator, and sets the resulting
   * string as the page title using the Title service from '@angular/platform-browser'.
   */
  setPageTitle(): void {
    this.titleService.setTitle('Shopka' + ' | ' + this.pageTitle);
  }
}
