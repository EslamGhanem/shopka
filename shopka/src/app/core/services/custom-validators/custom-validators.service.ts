import { AbstractControl, ValidationErrors } from '@angular/forms';
import validator from 'validator';

export class CustomValidatorsService {
  constructor() {}
  /**
   * Validates whether the input control value is a valid email address using the
   *  validator.js library.
   *
   * @param control The input control to validate.
   * @returns A validation error object { notEmail: true } if the input value is not a valid email
   * address, or null if validation passes.
   */
  // Check If Input Contains Valid Email
  static isEmail(control: AbstractControl): ValidationErrors | null {
    if (!control.value) {
      return null;
    }
    const value = control.value.trim() as string;
    if (value && !validator.isEmail(value)) {
      return { notEmail: true };
    }
    return null;
  }

    /**
   * Validates whether the input control value contains only Arabic or English characters and
   * numbers using the validator.js library.
   *
   * @param control The input control to validate.
   * @returns A validation error object { notAlphabet: true } if the input value contains non-Arabic and non-English
   * characters, or null if validation passes.
   */

  // Check If Input Contains Arabic OR English Characters Only
  static isAlphabet(control: AbstractControl): ValidationErrors | null {
    if (!control.value) {
      return null;
    }
    const value = control.value.trim() as string;
    const isValidAlphabetEn = value
      .split(' ')
      .every((word) => validator.isAlpha(word, 'en-US'));
    const isValidAlphabetAr = value
      .split(' ')
      .every((word) => validator.isAlpha(word, 'ar-EG'));
    if (value && !isValidAlphabetEn && !isValidAlphabetAr) {
      return { notAlphabet: true };
    }
    return null;
  }
}
