import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class LocalStorageService {
  constructor() {}
  /**
   * Gets the value stored in local storage for the specified key.
   *
   * @param key The key to retrieve the value for.
   * @returns The value stored in local storage for the specified key, or null
   * if no value is found.
   */

  // Get data from Local storage
  getLocal(key: string): any {
    const shopkaKey = 'key-' + key;
    const data = window.localStorage.getItem(shopkaKey);
    if (data) {
      return JSON.parse(data);
    } else {
      return null;
    }
  }

  /**
   * Sets the value for the specified key in local storage.
   *
   * @param key The key to set the value for.
   * @param value The value to store in local storage.
   */

  // set data into Local storage
  setLocal(key: string, value: any): void {
    const shopkaKey = 'key-' + key;
    const data = value === undefined ? '' : JSON.stringify(value);
    window.localStorage.setItem(shopkaKey, data);
  }

  /**
   * Removes all items in local storage except for the user language setting.
   */

  /* Remove All Locals Except User Lang */
  removeAllLocals(): void {
    for (const key in window.localStorage) {
      if (
        window.localStorage.hasOwnProperty(key) &&
        key !== 'shopka-userLang'
      ) {
        window.localStorage.removeItem(key);
      }
    }
  }
}
