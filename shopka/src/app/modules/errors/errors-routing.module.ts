import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
  Error403Component,
  Error404Component,
  Error500Component,
} from './pages';

const routes: Routes = [
  {
    path: '403',
    component: Error403Component,
    data: { title: '403' },
  },
  {
    path: '404',
    component: Error404Component,
    data: { title: 'Page not found' },
  },
  {
    path: '500',
    component: Error500Component,
    data: { title: 'Server Error' },
  },
  { path: '', redirectTo: '404', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ErrorsRoutingModule {}
