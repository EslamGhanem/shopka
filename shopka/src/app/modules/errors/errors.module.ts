import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ErrorsRoutingModule } from './errors-routing.module';
import {
  Error403Component,
  Error404Component,
  Error500Component,
} from './pages';

@NgModule({
  declarations: [Error403Component, Error404Component, Error500Component],
  imports: [CommonModule, ErrorsRoutingModule],
})
export class ErrorsModule {}
