import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductRoutingModule } from './product-routing.module';
import { ProductItemComponent } from './components';
import { ProductGridComponent, ProductDetailsComponent } from './pages';

@NgModule({
  declarations: [
    ProductItemComponent,
    ProductGridComponent,
    ProductDetailsComponent,
  ],
  imports: [CommonModule, ProductRoutingModule],
})
export class ProductModule {}
