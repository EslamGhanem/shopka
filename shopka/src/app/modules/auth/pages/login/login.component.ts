import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { CustomValidatorsService } from 'src/app/core/services/custom-validators/custom-validators.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm!: FormGroup;
  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.initForm();
  }

  // create From
  initForm(): void {
    this.loginForm = this.fb.group({
      email: new FormControl(null, {
        validators: [Validators.required, CustomValidatorsService.isEmail],
      }),
      password: new FormControl(null, {
        validators: [Validators.required, Validators.minLength(6)],
      }),
    });
  }

  //  * Returns an object containing all the controls of the login form.
  get formContarols() {
    return this.loginForm.controls;
  }

  /**
   * Performs a login action.
   * If the login form is invalid, it marks all the form controls as touched and returns.
   * Otherwise, it creates a new object `body` containing all the form values and resets the form.
   * And then send it to server .
   *
   * @returns {void}
   */
  login(): void {
    if (this.loginForm.invalid) {
      this.loginForm.markAllAsTouched();
      return;
    }
    const body = {
      ...this.loginForm.value,
    };

    // Here You can use Http to send Body data to server

    this.loginForm.reset();
  }
}
